#!/bin/sh
# For deploying from /var/www
# Just git clone onto /var/www and have fun.
# Directory name in /var/www will be name for a2ensite config file and <project> in hostname/<project>

PROJECT_PATH=$(pwd)
echo "Project path is $PROJECT_PATH"
A=$(basename $PWD)
cd ..
sudo chmod -R 755 $A
# may also be as A=`basename $PWD`
echo "Project name is $A"
apache2 -v
echo "Reading main Apache2 config (/etc/apache2/apache2.conf)... Please edit this file if it is not OK"
echo "prior to 2.3.11, NameVirtualHost is required to instruct the server that a particular IP address and port combination was usable as a name-based virtual host"
echo "Directive IncludeOptional (Available in 2.3.6 and later) or Include (2.0.41 and later) must be uncomment (especially sites-enabled and mods-enabled). Grep Include in apache2.conf..."
echo "========================================="
cat /etc/apache2/apache2.conf | grep Include
echo "========================================="
echo "Manually add NameVirtualHost and IncludeOptional directive if it's needed"
echo "========================================="
ls -la | grep $A
echo "========================================="
cd $PROJECT_PATH
echo "Preparing virtualhost config file, which name is $A.conf..."
REPLACEMENT_KEY="project"
NEW_VALUE=$A
PATH_TO_R_CONFIG_FILE="$PWD/deploy/project.conf"
sudo sed -e "s/$REPLACEMENT_KEY/$NEW_VALUE/" $PATH_TO_R_CONFIG_FILE > "$PWD/deploy/$A.conf"
echo "New virtualhost file created succesfully"
echo "========================================="
sudo cp $PROJECT_PATH/deploy/$A.conf /etc/apache2/sites-available/$A.conf
cd /etc/apache2/sites-available
sudo chmod a+rx $A.local.conf
echo "Reading /etc/apache2/sites-available catalog..."
echo "========================================="
ls -la | grep $A
echo "========================================="
sudo a2ensite $A.conf
sudo service apache2 reload
sudo service apache2 restart

